﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SibintekTZ
{
    public abstract class AbstractTelephone // abstract Telephone class
    {
        protected AbstractTelephone(string imei) // protect imei from overwrite
        {
            ImeiNumber = imei;
        }
        public string ImeiNumber { get; } // IMEI number
        public string SimNumber { get; set; } // SIM number
        public Phonebook Phonebook { get; set; } // link to class Phonebook
        public virtual void Connect(AbstractBaseStation baseStation) // connection to base station
        {
            baseStation.RegisterTelephone(this);
        }

        public virtual void CallPhone(Contact contact) // call by contact from phonebook
        {
            Console.WriteLine($"Call to contact {contact.Name}...");
            CallPhone(contact.PhoneNumber);
        }
        public virtual void CallPhone(string phoneNumber) // call by phone number
        {
            Console.WriteLine($"Call by phone number: {phoneNumber}...");
        }
    }
}
