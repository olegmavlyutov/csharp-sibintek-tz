﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SibintekTZ
{
    public class Telephone3G : AbstractTelephone // Telephone with 3G constructor
    {
        public Telephone3G(string imei) : base(imei) // constructor with protected IMEI
        {
        }
        public override void Connect(AbstractBaseStation baseStation) // connection to 3G base station
        {
            Console.WriteLine($"3G - connected to BaseStation: {baseStation}");
        }

        public override void CallPhone(Contact contact) // call by contact name from phonebook
        {
            Console.WriteLine($"3G - call to contact {contact.Name}...");
            CallPhone(contact.PhoneNumber);
        }
        public override void CallPhone(string phoneNumber) // call by phone number
        {
            Console.WriteLine($"3G - call by phone number: {phoneNumber}...");
        }
    }
}
