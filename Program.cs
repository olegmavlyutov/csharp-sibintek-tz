﻿using System;

namespace SibintekTZ
{
    class Program
    {
        static void Main(string[] args)
        {
            Telephone testPhone = new Telephone("12345") // testPhone create - takes imei and sim
            {
                SimNumber = "54321"
            };

            Telephone3G testPhone3G = new Telephone3G("23456") // testPhone3G create - takes imei and sim
            {
                SimNumber = "65432"
            };

            BaseStation testBaseStation = new BaseStation(); // testBaseStation create

            BaseStation3G testBaseStation3G = new BaseStation3G(); // testBaseStation3G create

            Contact testContact = new Contact // testContact create with name and phone number
            {
                    Name = "John Doe",
                    PhoneNumber = "345678"
            };

            // regular telephones tests
            Console.WriteLine("Please, press any key to test telephones without 3G");
            Console.ReadKey();
            Console.WriteLine("\n");

            testPhone.Connect(testBaseStation);
            testBaseStation.RegisterTelephone(testPhone);
            testPhone.CallPhone(testContact);
            testPhone.CallPhone("789");

            // 3G telephones tests
            Console.WriteLine("\nPlease, press any key to test telephones with 3G \n");
            Console.ReadKey();
            Console.WriteLine("\n");

            testPhone3G.Connect(testBaseStation3G);
            testBaseStation3G.RegisterTelephone(testPhone3G);
            testPhone3G.CallPhone(testContact);
            testPhone3G.CallPhone("890");

            Console.ReadKey();
        }
    }
}
