﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace SibintekTZ
{
    public class BaseStation3G : AbstractBaseStation // 3G base station class
    {
        public override void RegisterTelephone(AbstractTelephone telephone) // register process for 3G telephone
        {
            Console.WriteLine($"3G - registered telephone {telephone}");
        }
    }
}
