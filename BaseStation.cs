﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SibintekTZ
{
    public class BaseStation : AbstractBaseStation // regular BaseStation class
    {
        public override void RegisterTelephone(AbstractTelephone telephone) // register process for regular telephone
        {
            Console.WriteLine($"Registered telephone: {telephone}");
        }
    }
}
