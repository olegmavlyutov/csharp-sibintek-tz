﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SibintekTZ
{
    public class Phonebook // this class contains contacts inside
    {
        public IList<Contact> Contacts { get; } = new List<Contact>(); // collection of Contacts
    }
}
