﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SibintekTZ
{
    public class Telephone : AbstractTelephone // Telephone without 3G constructor
    {
        public Telephone(string imei) : base(imei) // constructor with protected IMEI
        {
        }
        public override void Connect(AbstractBaseStation baseStation) // connection to base station
        {
            Console.WriteLine($"Connected to BaseStation: {baseStation}");
        }
    }
}
