﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SibintekTZ
{
    public class Contact // common contact class
    {
        public string Name { get; set; } // contact's name
        public string PhoneNumber { get; set; } //contact's phone number
    }
}
