﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SibintekTZ
{
    public abstract class AbstractBaseStation
    {
        public virtual void RegisterTelephone(AbstractTelephone telephone)
        {
            Console.WriteLine($"{GetType().Name}: {telephone.GetType().Name} is not recognized"); // if wrong input data
        }
    }
}